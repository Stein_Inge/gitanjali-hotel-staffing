﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utility;

namespace HotelRecruitment.Controllers
{
    public class AccountServiceController : ApiController
    {
        HotelManagementEntities dbContext = new HotelManagementEntities();
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }
        public bool Post([FromBody] DAL.User model)
        {
            string password = CommonFunction.Encrypt(model.Password);
            return dbContext.Users.Any(x => x.Email == model.Email && x.Password == password);
        }

        // POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}