﻿using DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace HotelRecruitment.Controllers
{
    public class JobSeekerServiceController : ApiController
    {
        private static readonly string ServerUploadFolder = "C:\\Temp"; //Path.GetTempPath();
        HotelManagementEntities dbContext = new HotelManagementEntities();
        // GET api/jobseeker1
        public IEnumerable<JobSeeker> Get()
        {
            return dbContext.JobSeekers.Where(x => x.IsDeleted == false).ToList().OrderByDescending(x => x.CreatedOn);
        }

       
        public void Post(JobSeeker model)
        {



            if (model.Id > 0)
            {
                var jobSeeker = dbContext.JobSeekers.Where(x => x.Id == model.Id).FirstOrDefault();
                jobSeeker.Rating = Convert.ToInt32(model.Rating);
                jobSeeker.Feedback = model.Feedback;
                jobSeeker.IsInterview = model.IsInterview;
                jobSeeker.IsActive = model.IsActive;
                dbContext.SaveChanges();
            }
            
        }

        // GET api/jobseeker1/5
        public JobSeeker Get(int id)
        {
            return dbContext.JobSeekers.Where(x => x.Id == id).FirstOrDefault();
        }

        // POST api/jobseeker1
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT api/jobseeker1/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/jobseeker1/5
        public void Delete(int id)
        {
        }
    }
}
